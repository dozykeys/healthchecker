

	<?php include("views/components/header.php"); ?>


		<div class="content">
			<div class="container-fluid">
				<div class="row">

					<div class="col-md-12">
						<div class="doc-review review-listing custom-edit-service">
						

							<div class="card">
								<div class="card-body">
									<h3 class="pb-3">Select Symptoms</h3>
									<form method="post" autocomplete="off" id="formee">
									
										<div class="service-fields mb-3">
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<label>Select Symptoms <span class="text-danger">*</span></label>
														<select placeholder="Select symptom" class="form-select form-control demo-default" id="symptoms" name="symptom[]" multiple required>
														</select>
													</div>
												</div>
											
											</div>
										</div>

										<div class="service-fields mb-3">
											<div class="row">
												<div class="col-md-6 avail-time">
													<div class="mb-3">
														<div class="schedule-calendar-col justify-content-start">
															<form class="d-flex flex-wrap">
															<label>Date Of Birth<span class="text-danger">*</span></label>
																<div class="me-3">
																	<input type="date" class="form-control" name="year" id="schedule_date" value="<?= date('Y-m-d')  ?>" min="1900-05-21">
																</div>
															</form>
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label>Gender <span class="text-danger">*</span></label>
														<select class="form-select form-control" name="gender" id="gender">
														<option value="male">Male</option>
                                                         <option value="female">Female</option>
														</select>
													</div>
												</div>
											</div>
										</div>


										<div class="submit-section">
											<button class="btn btn-primary" id="submit" value="submit">Submit</button>
										</div>
									</form>
								</div>
							</div>

							<div class="card" id="reach">
								<div class="card-body">
								<h3 class="pb-3">Diagnosis Result</h3>
								<div class="card card-table mb-0">
											<div class="card-body">
												<div class="table-responsive">
													<table class="table table-hover table-center mb-0">
														<thead>
															<tr>	
															<th></th>													
																<th>Name</th>
																<th>Professional Name</th>
															
															</tr>
														</thead>
														<tbody id="diagnosis-result">
														
														
															
														</tbody>
													</table>
												</div>
											</div>
										</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

	
		<?php include("views/components/footer.php"); ?>


</body>

</html>