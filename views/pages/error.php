<?php include("views/components/header.php"); ?>

<div class="error-page">
<div class="error-box">
<h1>404</h1>
<h3 class="h2 mb-3"><i class="fa fa-warning"></i> Oops! Page not found!</h3>
<p class="h4 font-weight-normal">The page you requested was not found.</p>
<a href="index" class="btn btn-primary">Back to Home</a>
</div>
</div>


<?php include("views/components/footer.php"); ?>


</body>

</html>