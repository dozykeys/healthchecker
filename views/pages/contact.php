

	<?php include("views/components/header.php"); ?>


		<section class="contact-section">
			<div class="container">
				<div class="row mb-5">
					<div class="col-md-12 text-center">
						<h3 class="mb-4">Contact Us</h3>
						<p>We know you care to ask questions, that's why we are here.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 d-flex">
						<div class="contact-box flex-fill">
							<div class="infor-img">
								<div class="image-circle">
									<i class="feather-phone"></i>
								</div>
							</div>
							<div class="infor-details text-center">
								<label>Phone Number</label>
								<p>+2347061034439</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 d-flex">
						<div class="contact-box flex-fill">
							<div class="infor-img">
								<div class="image-circle bg-primary">
									<i class="feather-mail"></i>
								</div>
							</div>
							<div class="infor-details text-center">
								<label>Email</label>
								<p><a href="mailto:info@seamhealthgroup.com" class="__cf_email__">info@seamhealthgroup.com</a></p>
							</div>
						</div>
					</div>
					<div class="col-md-4 d-flex">
						<div class="contact-box flex-fill">
							<div class="infor-img">
								<div class="image-circle">
									<i class="feather-map-pin"></i>
								</div>
							</div>
							<div class="infor-details text-center">
								<label>Location</label>
								<p>Elkridge Maryland 21075 USA</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="contact-form">
			<div class="container">
				<div class="section-header text-center">
					<h2>Get in touch!</h2>
				</div>
				<div class="row">
					<div class="col-md-12">
						<form>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Your Name <span>*</span></label>
										<input type="text" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Your Email <span>*</span></label>
										<input type="text" class="form-control">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>Subject</label>
										<input type="text" class="form-control">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>Comments <span>*</span></label>
										<textarea class="form-control">

											</textarea>
									</div>
								</div>
							</div>
							<button class="btn bg-primary">Send Message</button>
						</form>
					</div>
				</div>
			</div>
		</section>


		<section class="contact-map d-flex">
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d49466.652370148826!2d-76.7475103!3d39.2050426!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89b7e1ee4db7845b%3A0x7c73e169f9cb61!2sElkridge%2C%20MD%2021075%2C%20USA!5e0!3m2!1sen!2sng!4v1639742582899!5m2!1sen!2sng" allowfullscreen="" loading="lazy"></iframe>
		</section>

	<?php include("views/components/footer.php"); ?>


</body>

</html>