	<footer class="footer">

		<div class="footer-top">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-4">

						<div class="footer-widget footer-about">

							<div class="footer-about-content">
								<p>A web-based medical diagnosis app using a publicly available medical diagnosis API from <a href="https://apimedic.com/" style="color:white;text-decoration:underline;">APIMEDIC</a></p>
								<div class="social-icon">
									<ul>
										<li>
											<a href="https://web.facebook.com/seamhealth/?_rdc=1&_rdr" target="_blank"><i class="fab fa-facebook-f"></i> </a>
										</li>
										<li>
											<a href="https://twitter.com/seamhealth/" target="_blank"><i class="fab fa-twitter"></i> </a>
										</li>
										<li>
											<a href="https://www.linkedin.com/company/seamhealth/" target="_blank"><i class="fab fa-linkedin-in"></i></a>
										</li>
										<li>
											<a href="https://www.instagram.com/seamhealth/" target="_blank"><i class="fab fa-instagram"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>

					</div>
					<div class="col-md-4">

						<div class="footer-widget footer-menu">
							<h2 class="footer-title">Trusted Partners</h2>
							<ul>
								<li><img src="https://seamhealth.com/images/seamhealth_logo.png" alt=""></li>
							</ul>

						</div>

					</div>
					<div class="col-md-4">

						<div class="footer-widget footer-contact">
							<h2 class="footer-title">Contact Us</h2>
							<div class="footer-contact-info">
								<div class="footer-address">
									<span><i class="fas fa-map-marker-alt"></i></span>
									<p>Elkridge Maryland 21075 USA</p>
								</div>
								<p>
									<i class="fas fa-phone-alt"></i>
									+2347061034439
								</p>
								<p class="mb-0">
									<i class="fas fa-envelope"></i>
									<a href="mailto:info@seamhealthgroup.com" style="color:white;text-decoration:underline;" class="__cf_email__">info@seamhealthgroup.com</a>
								</p>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>


		<div class="footer-bottom">
			<div class="container-fluid">

				<div class="copyright">
					<div class="row">
						<div class="col-md-6 col-lg-6">
							<div class="copyright-text">
								<p class="mb-0">&copy; <?= date("Y"); ?> Health Checker. All rights reserved.</p>
							</div>
						</div>
						<div class="col-md-6 col-lg-6">

							<div class="copyright-menu">
								<ul class="policy-menu">
									<li><a href="contact">Contact</a></li>
								</ul>
							</div>

						</div>
					</div>
				</div>

			</div>
		</div>

	</footer>

	</div>

	<div class="modal fade" id="showIssue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="acc_title">Diagnosis Description</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<div class="card">
						<div class="card-body pt-0">

							<h3 class="pt-4" id="DName"></h3>
							<hr>


							<div class="tab-content pt-3">

								<div role="tabpanel" id="doc_overview" class="tab-pane fade show active">
									<div class="row">
										<div class="col-md-12">

											<div class="widget about-widget">
												<h4 class="widget-title">Description</h4>
												<p id="Description"></p>
											</div>

											<div class="widget about-widget">
												<h4 class="widget-title">Specialization</h4>
												<p id="spec_"></p>
											</div>


											<div class="widget awards-widget">
												<h4 class="widget-title">Possible Symptoms</h4>
												<div class="experience-box">
													<ul class="experience-list" id="psymptoms">

													</ul>
												</div>
											</div>


											<div class="widget about-widget">
												<h4 class="widget-title">Medical Condition</h4>
												<p id="MedicalCondition">Store this syrup at room temperature protected from sunlight, heat, and moisture. Keep away from reaching out of children and pets. Ensure that the unused medicine is disposed of properly.</p>
											</div>

											<div class="widget about-widget">
												<h4 class="widget-title">Treatment Description</h4>
												<p id="tdescription"></p>
											</div>

										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary si_accept_cancel" data-bs-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="incomplete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="acc_title">Invalid Selection</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<p id="acc_msg">It appears you have not selected any symptom yet</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary si_accept_cancel" data-bs-dismiss="modal">OK</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="noDiagnosis" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="acc_title">No Diagnosis Found</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<p id="acc_msg">You can reduce the number of symptoms entered to ensure better diagnosis</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary si_accept_cancel" data-bs-dismiss="modal">OK</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="noIssue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="acc_title">No Issue Description Found</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<p id="acc_msg">There were no issue descriptions for the selected diagnosis.Please check back.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary si_accept_cancel" data-bs-dismiss="modal">OK</button>
				</div>
			</div>
		</div>
	</div>



	<script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
	<script src="assets/js/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="assets/js/selectize.js"></script>

	<script src="assets/js/bootstrap.bundle.min.js"></script>


	<script src="assets/js/script.js"></script>

	<script>
		function viewDiagnosis(issueid,dd) {
			
			customLoading();
			$.ajax({
				url: 'controllers/fetch-issue.php',
				type: 'GET',
				data: {
					issueid
				},
				success: function(res) {
					customLoading('hide', 'slow');
					let sy2 = "";
					dd.split(",").forEach(x => {
							sy2 += `<a class="badge rounded-pill btn btn-sm bg-success-light">${x}</a>`;
						});
						$("#spec_").html(sy2);
					if (res) {
						let sy = "";
						res.PossibleSymptoms.split(",").forEach(x => {
							sy += `<li>
                            <div class="experience-user">
                                <div class="before-circle"></div>
                            </div>
                            <div class="experience-content">
                                <div class="timeline-content">
                                    <p>${x}</p>
                                </div>
                            </div>
                        </li>`;
						});

						$("#psymptoms").html(sy);
						$("#DName").text(res.Name);
						$("#Description").text(res.Description + res.DescriptionShort);
						$("#MedicalCondition").text(res.MedicalCondition);
						$("#tdescription").text(res.TreatmentDescription);
						$('#showIssue').modal('show');
					} else {
						$('#noIssue').modal('show');
					}

				}
			});
		}


		$(function() {
			$.ajax({
				url: 'controllers/fetch-symptoms.php',
				type: 'GET',
				success: function(data) {
					var html = '';
					$.each(data, function(key, value) {
						html += '<option value="' + value.ID + '">' + value.Name + '</option>';
					});
					$('#symptoms').html(html).selectize({
						plugins: ["remove_button"],
						maxItems: 10,
					});
					customLoading('hide', 'slow');
				}
			});
			$('#submit').click(function(event) {
				event.preventDefault();
				if (!$("#formee")[0].checkValidity()) {
					$('#incomplete').modal('show');
					return;
				}
				customLoading();
				$.ajax({
					url: 'controllers/fetch-diagnosis.php',
					type: 'GET',
					data: $("#formee").serialize(),
					success: function(data) {
						customLoading('hide', 'slow');
						if (data.length > 0) {

							let dfeed = "";
							for (let i = 0; i < data.length; i++) {
								let specsLength = data[i].Specialisation.length;
								let specs = data[i].Specialisation;
								// let spec = ``;
								let spec = [];
								if (specsLength > 0) {								
									for (let j = 0; j < specsLength; j++) {
										spec.push(specs[j].Name);

									}
								}
								dfeed += `<tr>		
								<td class="text-end">
                                        <div class="table-action">
                                            <a href="#" ${spec} onclick="viewDiagnosis(${data[i].Issue.ID},'${spec}')" class="btn btn-sm bg-info-light">
                                                <i class="far fa-eye"></i> View
                                            </a>
                                        </div>
                                    </td>												
                                    <td>${data[i].Issue.Name}</td>
                                    <td>${data[i].Issue.ProfName}</td>                                    
                                </tr>`;
							}
							$("#diagnosis-result").html(dfeed);
							var $anchor = $('#reach').offset();
							window.scrollTo($anchor.left, $anchor.top);

						} else {
							$('#noDiagnosis').modal('show');
						}
					}
				});
			});
		})
	</script>