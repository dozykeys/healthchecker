<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Health Checker</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <link href="assets/img/favicon.png" rel="icon">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/selectize.css" />
  
    <script>
        function customLoading(action, speed) {
            if (!document.querySelector('#custom-loading')) {
                document.body.innerHTML += `<div id="custom-loading" style="
    display: none;
    opacity: 0;
    text-align: center;
    position: fixed;
    top: 0;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    left: 0;
    z-index: 99999999;
    margin: auto;
    width: 100vw;
    height: 100vh;
    overflow: hidden;
    background-color: rgba(255, 255, 255, 0.95);
">
    <img id="prepre" 
    src="assets/img/loading.gif">
</div>`
            }
            if (action === 'hide') {
                if (speed) {
                    if (speed === 'fast') {
                        document.querySelector('#custom-loading').style.transition = "opacity 100ms"
                        document.querySelector('#custom-loading').style.opacity = "0"
                        setTimeout(() => {
                            document.querySelector('#custom-loading').style.display = "none"
                        }, 100);
                    } else if (speed === 'slow') {
                        document.querySelector('#custom-loading').style.transition = "opacity 2s"
                        document.querySelector('#custom-loading').style.opacity = "0"
                        setTimeout(() => {
                            document.querySelector('#custom-loading').style.display = "none"
                        }, 2000);
                    }
                    return;
                }
                document.querySelector('#custom-loading').style.display = "none"
                return;
            }
            document.querySelector('#custom-loading').style.display = "flex"
            document.querySelector('#custom-loading').style.opacity = "1"
        }
    </script>
</head>

<body>
    <div id="custom-loading" style="
    display: flex;
    opacity: 1;
    text-align: center;
    position: fixed;
    top: 0;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    left: 0;
    z-index: 99999999;
    margin: auto;
    width: 100vw;
    height: 100vh;
    overflow: hidden;
    background-color: rgba(255, 255, 255, 0.95);
">
        <img id="prepre" src="assets/img/loading.gif">
    </div>

    <div class="main-wrapper">


        <div class="breadcrumb-bar">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-md-12 col-12">
                        <nav aria-label="breadcrumb" class="page-breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index">Home</a></li>
                                <li class="breadcrumb-item" aria-current="page"><a href="contact">Contact Us</a></li>
                            </ol>
                        </nav>
                        <h2 class="breadcrumb-title">Health Checker</h2>
                    </div>
                </div>
            </div>
        </div>