<?php
header("content-type:application/json");
require_once(dirname(__FILE__,2). '/libs/init.php');
$gender=$_GET['gender'];
$yearOfBirth=date('Y',strtotime($_GET['year']));
$selectedSymptoms=$_GET['symptom'];
die(json_encode($medic->loadDiagnosis($selectedSymptoms, $gender, $yearOfBirth)));