# HEALTH CHECKER

![Yarn start:dev](assets/img/image.png)


This would be implementing:

- A web-based medical diagnosis app using a publicly available medical diagnosis API from https://apimedic.com/.

This repository contains source code for the implementation of a medical diagnosis app

## Table of contents

- [Getting started](#getting-started)
- [Prerequisites](#prerequisites)
- [Installation](#installation)

## Getting started

These instructions will help you get you a copy of the project up and running on your local machine for development and testing purposes.

```
git clone https://gitlab.com/dozykeys/healthchecker.git
```

## Prerequisites

Prerequisites to install include Apache,Mysql,PHP.

## Installation

You will need to drop this folder in your server's root directory.

## Program Launch (Url)

This will get the application up and running.

```
127.0.0.1/healthchecker/
```

To view the hosted demo, simply visit
https://esolutionsforcare.com/symptom/