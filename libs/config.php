<?php
/**
 * @author Duru Chidozie
 * @version 1.0
 */
const API_NAME = 'Health Checker';
const API_VERSION = '1.0';
//Switch for localhost and live server
switch ($_SERVER['REMOTE_ADDR']) {
    case '127.0.0.1':
    case '::1':
        error_reporting(E_ALL);

        //Database Credentials
        define('DB_HOST', 'localhost');
        define('DB_USER', 'root');
        define('DB_PASS', '');
        define('DB_NAME', 'healthchecker');
        define('IS_DEVELOPMENT', true);
        //Token details
        define("MEDIC_USERNAME", "speedyprimer0@gmail.com");
        define("MEDIC_PASSWORD", "Tz93ApNb2d6E4Yoc5");
        define("MEDIC_AUTH_URL", 'https://sandbox-authservice.priaid.ch/login');
        define("MEDIC_API_URL", 'https://sandbox-healthservice.priaid.ch/');
        break;
    default:
        //Arbitrary Root Dir
        define("ROOT_DIR", '/');
        //Database Credentials
        define("DB_HOST", 'localhost');
        define('DB_USER', '');
        define('DB_PASS', ''); //
        define('DB_NAME', '');
        define('IS_DEVELOPMENT', false);
        //Token details
        define("MEDIC_USERNAME", "Bm4p2_GMAIL_COM_AUT");
        define("MEDIC_PASSWORD", "Rs5i9TNn46Fyf7EQw");
        define("MEDIC_AUTH_URL", 'https://authservice.priaid.ch/login');
        define("MEDIC_API_URL", 'https://healthservice.priaid.ch/');
        break;
}

///FOR JERRY & AYAFA CLASS
define('DB_DSN', 'mysql:host=localhost;port=3306;dbname=' . DB_NAME);
define('DB_USERNAME', DB_USER);
define('DB_PASSWORD', DB_PASS);

//Database Options
const DB_DRIVER = 'mysql';

// Create a connection, once only.
$config = [
    'driver'    => DB_DRIVER, // Db driver
    'host'      => DB_HOST,
    'database'  => DB_NAME,
    'username'  => DB_USER,
    'password'  => DB_PASS,
    'charset'   => 'utf8mb4', // Optional
    'collation' => 'utf8mb4_0900_ai_ci', // Optional
    //'prefix'    => 'cb_', // Table prefix, optional
    'options'   => [
        // PDO constructor options, optional
        PDO::ATTR_TIMEOUT          => 5,
        PDO::ATTR_EMULATE_PREPARES => false,
    ],
];



//Database Tables
