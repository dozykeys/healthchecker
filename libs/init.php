<?php
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('Africa/Lagos');
require_once("config.php");


spl_autoload_register(function ($class_name) {
    include  $class_name . '.php';
});

$medic=new DiagnosisClient(MEDIC_USERNAME, MEDIC_PASSWORD, MEDIC_AUTH_URL, MEDIC_API_URL);

